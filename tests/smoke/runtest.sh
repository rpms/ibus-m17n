#!/bin/bash
cd ../source

echo "---Start autogen.sh---"
NOCONFIGURE=1 ./autogen.sh
echo "---End autogen.sh---"
echo "--------------------"

./configure --disable-static --with-gtk=3.0
make check
